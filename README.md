使用 rust 程序读取 android 应用统计信息，格式化输出并插入到 mysql 数据库中。

此项目为 rust 初学者练习用。

#### 1.将 android 系统中的统计文件拉取到本地
```shell script
adb pull /data/system/usagestats/ .
```

#### 2.执行本分析程序
```shell script
# 第一个参数为文件夹路径
# 第二个参数为是否插入到数据库中 (true/false)
./android-usage-state-xml-parser-rs ./usagestats false
```

>如果需要插入到数据库，请先修改 db_helper.rs 中的数据库配置信息
```rust
pub fn init_pool() -> Pool {
    mysql::Pool::new("mysql://user_name:password@ip:port/database_name").unwrap()
}
```
