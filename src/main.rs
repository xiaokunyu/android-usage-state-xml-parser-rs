#[macro_use]
extern crate mysql;

use std::env;
use std::{fs, io};
use std::borrow::BorrowMut;
use std::fs::DirEntry;
use std::path::Path;
use std::str::FromStr;

use chrono::{FixedOffset, TimeZone};
use minidom::Element;
use quick_xml::Reader;

use crate::db_helper::Usage;
use std::io::{Error, ErrorKind};

mod db_helper;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        return Err(Error::new(ErrorKind::InvalidInput, "请输入文件夹参数"));
    }

    let dir_path = &args[1];
    let mut insert_db = false;
    if args.len() == 3 {
        insert_db = bool::from_str(&args[2]).unwrap_or_default();
    }

    let mut pool = Option::None;
    if insert_db {
        pool = Some(db_helper::init_pool());
        db_helper::init_table(pool.as_ref().unwrap());
    }

    let path_str = String::from(dir_path);

    visit_dirs(Path::new(&path_str), &|entry| {
        let path = entry.path();
        let file_name = path.file_name().expect("文件名错误").to_str().unwrap();
        let parent_name = path.parent().expect("文件路径错误").file_name().unwrap().to_str().unwrap();

        match file_name {
            "version" => {
                let content = fs::read_to_string(&path).expect(format!("读取文件[{}]内容失败", file_name).as_str());
                println!("\n版本信息：\n{}", content);
            }
            _ => {
                let base_time = i64::from_str(file_name).expect("文件名不正确"); // 文件名转成基础时间

                let mut reader = Reader::from_file(&path).expect(format!("读取文件[{}]内容失败", file_name).as_str());
                let usage_stats = Element::from_reader(reader.borrow_mut()).unwrap();

                let version = usage_stats.attr("version").unwrap();
                let end_time = i64::from_str(usage_stats.attr("endTime").unwrap()).unwrap();

                let usage_time = format_time(base_time + end_time); // 文件名（统计时间）

                match parent_name {
                    "daily" => println!("\n每日统计时间 : {:?}", usage_time),
                    "weekly" => println!("\n每周统计时间 : {:?}", usage_time),
                    "monthly" => println!("\n每月统计时间 : {:?}", usage_time),
                    "yearly" => println!("\n每年统计时间 : {:?}", usage_time),
                    _ => (),
                }

                for child in usage_stats.children() {
                    match child.name() {
                        "packages" => {
                            for package in child.children() {
                                let last_time_active = i64::from_str(package.attr("lastTimeActive").unwrap()).unwrap();
                                let package_name = package.attr("package").unwrap();
                                let time_active = i64::from_str(package.attr("timeActive").unwrap()).unwrap() / 1000;
                                let last_event = u32::from_str(package.attr("lastEvent").unwrap()).unwrap();

                                println!("{}：上次使用时间：{}，总共使用时长：{}s，最后操作：{}", package_name, format_time(base_time + last_time_active), time_active, format_event_type(last_event));

                                if insert_db {
                                    let mut usage = Usage::new();
                                    usage.set_source(parent_name.to_string());
                                    usage.set_usage_time(usage_time.to_string());
                                    usage.set_usage_type(package.name().to_string());
                                    usage.set_package(package_name.to_string());
                                    usage.set_last_time_active(format_time(base_time + last_time_active));
                                    usage.set_time_active(time_active.to_string());
                                    usage.set_last_event(format_event_type(last_event).to_string());

                                    db_helper::insert(pool.as_ref().unwrap(), &usage);
                                }
                            }
                        }
                        "configurations" => {
                            for config in child.children() {}
                        }
                        "event-log" => {
                            for event in child.children() {
                                let event_time = i64::from_str(event.attr("time").unwrap()).unwrap();
                                let package_name = event.attr("package").unwrap();
                                let class_name = event.attr("class").unwrap_or_default();
                                let event_type = u32::from_str(event.attr("type").unwrap()).unwrap();

                                println!("{}/{} 在 {} {}", package_name, class_name, format_time(base_time + event_time), format_event_type(event_type));

                                if insert_db {
                                    let mut usage = Usage::new();
                                    usage.set_source(parent_name.to_string());
                                    usage.set_usage_time(usage_time.to_string());
                                    usage.set_usage_type(event.name().to_string());
                                    usage.set_package(package_name.to_string());
                                    usage.set_class(class_name.to_string());
                                    usage.set_last_time_active(format_time(base_time + event_time));
                                    usage.set_last_event(format_event_type(event_type).to_string());

                                    db_helper::insert(pool.as_ref().unwrap(), &usage);
                                }
                            }
                        }
                        _ => (),
                    }
                }
            }
        }
        Ok(())
    })
}

fn visit_dirs(dir: &Path, cb: &dyn Fn(&DirEntry) -> io::Result<()>) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                visit_dirs(&path, cb)?;
            } else {
                cb(&entry);
            }
        }
    }
    Ok(())
}

fn format_time(millis: i64) -> String {
    FixedOffset::east(8 * 3600).timestamp_millis(millis).format("%Y-%m-%d %H:%M:%S").to_string()
}

fn format_event_type(event_type: u32) -> &'static str {
    match event_type {
        0 => "-",
        1 => "MOVE_TO_FOREGROUND",
        2 => "MOVE_TO_BACKGROUND",
        3 => "END_OF_DAY",
        4 => "CONTINUE_PREVIOUS_DAY",
        5 => "CONFIGURATION_CHANGE",
        6 => "SYSTEM_INTERACTION",
        7 => "USER_INTERACTION",
        8 => "SHORTCUT_INVOCATION",
        9 => "CHOOSER_ACTION",
        _ => "UNKNOWN",
    }
}