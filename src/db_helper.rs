use mysql::Pool;

#[derive(Debug, PartialEq, Eq)]
pub struct Usage {
    source: String,
    usage_time: String,
    usage_type: String,
    package: String,
    class: String,
    last_time_active: String,
    time_active: String,
    last_event: String,
}

impl Usage {
    pub fn new() -> Self {
        Usage {
            source: String::new(),
            usage_time: String::new(),
            usage_type: String::new(),
            package: String::new(),
            class: String::new(),
            last_time_active: String::new(),
            time_active: String::new(),
            last_event: String::new(),
        }
    }

    pub fn set_source(&mut self, source: String) {
        self.source = source;
    }
    pub fn set_usage_time(&mut self, usage_time: String) {
        self.usage_time = usage_time;
    }
    pub fn set_usage_type(&mut self, usage_type: String) {
        self.usage_type = usage_type;
    }
    pub fn set_package(&mut self, package: String) {
        self.package = package;
    }
    pub fn set_class(&mut self, class: String) {
        self.class = class;
    }
    pub fn set_last_time_active(&mut self, last_time_active: String) {
        self.last_time_active = last_time_active;
    }
    pub fn set_time_active(&mut self, time_active: String) {
        self.time_active = time_active;
    }
    pub fn set_last_event(&mut self, last_event: String) {
        self.last_event = last_event;
    }
}

pub fn init_pool() -> Pool {
    mysql::Pool::new("mysql://user_name:password@ip:3306/database_name").unwrap()
}

pub fn init_table(pool: &Pool) {
    pool.prep_exec(r"CREATE TABLE IF NOT EXISTS usage_state(
                    source TEXT,
                    usage_time TEXT,
                    usage_type TEXT,
                    package TEXT,
                    class TEXT,
                    last_time_active TEXT,
                    time_active TEXT,
                    last_event TEXT
                )", ()).unwrap();
}

pub fn insert(pool: &Pool, usage: &Usage) {
    for mut stmt in pool.prepare(r"INSERT INTO usage_state
                                       (source, usage_time, usage_type, package, class, last_time_active, time_active, last_event)
                                   VALUES
                                       (:source, :usage_time, :usage_type, :package, :class, :last_time_active, :time_active, :last_event)").into_iter() {
        stmt.execute(params! {
                "source" => &usage.source,
                "usage_time" => &usage.usage_time,
                "usage_type" => &usage.usage_type,
                "package" => &usage.package,
                "class" => &usage.class,
                "last_time_active" => &usage.last_time_active,
                "time_active" => &usage.time_active,
                "last_event" => &usage.last_event,
            }).unwrap();
    }
}